import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment";
import { IGame } from '../models/game.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {


  constructor(
    private http: HttpClient
  ) { }

  getNominados(): Observable<IGame[]> {
    return this.http.get<IGame[]>(`${environment.url}/api/goty`);
  }

  votarJuego(idJuego: string) {
    return this.http.post(`${environment.url}/api/goty/${idJuego}`,null)
  }

}
