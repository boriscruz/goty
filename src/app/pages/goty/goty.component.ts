import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game.service';
import { IGame } from '../../models/game.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-goty',
  templateUrl: './goty.component.html',
  styleUrls: ['./goty.component.scss']
})
export class GotyComponent implements OnInit {

  juegos: IGame[] = [];

  constructor(
    private gameService: GameService
  ) { }

  ngOnInit() {
    this.gameService.getNominados().subscribe(juegos => this.juegos = juegos);
  }

  votarJuego(juego: IGame) {
    this.gameService.votarJuego(juego.id).subscribe((resp: any) => {
      if (resp.ok) Swal.fire('Gracias', resp.message, 'success');
    }, err => {
      if (err.error) Swal.fire('Ooops', err.error.message, 'error');
    });
  }
}
