import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from "rxjs/operators";
import { IGame } from '../../models/game.interface';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  results: any[] = [];

  constructor(
    private db: AngularFirestore
  ) { }

  ngOnInit() {
    this.db.collection('goty').valueChanges()
      .pipe(map((juegos: IGame[]) => juegos.map(({ name, votos }) => ({ name, value: votos }))))
      .subscribe(resp => this.results = resp)
  }

}
