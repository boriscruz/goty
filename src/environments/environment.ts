// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: 'http://localhost:5000/goty-db75b/us-central1',
  firebase: {
    apiKey: 'AIzaSyClrvICxssKXA8D1OizGDEhjhBfpLjvCss',
    authDomain: 'goty-db75b.firebaseapp.com',
    databaseURL: 'https://goty-db75b.firebaseio.com',
    projectId: 'goty-db75b',
    storageBucket: 'goty-db75b.appspot.com',
    messagingSenderId: '417279090270',
    appId: '1:417279090270:web:1867712777242f2d125ecf'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
